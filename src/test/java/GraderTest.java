import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GraderTest {
    @Test
    void fiftyFiveShouldReturnF() {
        var grader = new Grader();

        assertEquals('F', grader.determineLetterGrade(55));
    }

    @Test
    void sixtyThreeShouldReturnD() {
        var grader = new Grader();

        assertEquals('D', grader.determineLetterGrade(63));
    }

    @Test
    void seventyFourShouldReturnC() {
        var grader = new Grader();

        assertEquals('C', grader.determineLetterGrade(74));
    }

    @Test
    void eightyTwoShouldReturnB() {
        var grader = new Grader();

        assertEquals('B', grader.determineLetterGrade(82));
    }

    @Test
    void ninetyNineShouldReturnB() {
        var grader = new Grader();

        assertEquals('A', grader.determineLetterGrade(99));
    }

    //Edge scenarios
    @Test
    void fiftyShouldReturnF() {
        var grader = new Grader();

        assertEquals('F', grader.determineLetterGrade(50));
    }

    @Test
    void sixtyShouldReturnD() {
        var grader = new Grader();

        assertEquals('D', grader.determineLetterGrade(60));
    }

    @Test
    void seventyShouldReturnC() {
        var grader = new Grader();

        assertEquals('C', grader.determineLetterGrade(70));
    }

    @Test
    void eightyShouldReturnB() {
        var grader = new Grader();

        assertEquals('B', grader.determineLetterGrade(80));
    }

    @Test
    void ninetyShouldReturnB() {
        var grader = new Grader();

        assertEquals('A', grader.determineLetterGrade(90));
    }

    @Test
    void negativeNumberShouldReturnIllegalArgumentException() {
        var grader = new Grader();
        assertThrows(IllegalArgumentException.class, () -> grader.determineLetterGrade(-65));
    }
}