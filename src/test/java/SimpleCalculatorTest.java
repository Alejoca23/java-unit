import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SimpleCalculatorTest {

    @Test
    void twoPlusTwoShouldBeFour() {
        var calculator = new SimpleCalculator();
        assertEquals(4, calculator.add(2, 2));
    }

    @Test
    void fivePlusFiveShouldBeTen() {
        var calculator = new SimpleCalculator();
        assertEquals(10, calculator.add(5, 5));
        assertNotEquals(5, calculator.add(5, 5));
//        assertTrue();
//        assertFalse();
//        assertNull();
//        assertNotNull();
    }



}